#include"projet.h"
#include<time.h>

  HMM::HMM(): emission(new double*[3]), transition(new double*[3]), depart(new double[3]){ 
      for (int k=0;k<3;k++){
          emission[k]=new double[4];
      }
      for (int i=0;i<3;i++){
          for(int j=0;j<4;j++){
             emission[i][j]=0.25;
          }
     }

     
      for (int k=0;k<3;k++){
          transition[k]=new double[4];
      }
      for (int i=0;i<3;i++){
          for (int j=0;j<4;j++){
              transition[i][j]=0.25;
          }
      }
     

      for (int i=0;i<3;i++){
          depart[i]=0.33;
      }
     
  }


 HMM::HMM(double* depart, double **emission, double** transition): emission(emission), transition(transition), depart(depart){}

 void HMM::set_emission(double** e) {emission=e;}
 void HMM::set_transition(double** t) {transition=t;}

 double** HMM::get_emission()const {return emission;}
 double** HMM::get_transition()const {return transition;}
 double* HMM::get_depart()const {return depart;}

void HMM::afficher_emission ()const {
    cout<<"MATRICE D'EMISSION:"<<endl;
    cout<<"   A    C    T    G"<<endl;
    for(int i=1;i<4;i++){
        cout<<i<<"|";
        for(int j=0;j<4;j++){
            cout<<emission[i-1][j]<<"|";
        }
        cout<<endl;
    }
}

void HMM::afficher_transition ()const {
    cout<<"MATRICA DE TRANSITION:"<<endl;
    cout<<"   1    2    3    4"<<endl;
    for(int i=1;i<4;i++){
        cout<<i<<"|";
        for(int j=0;j<4;j++){
            cout<<transition[i-1][j]<<"|";
        }
        cout<<endl;
    }
}

void HMM::afficher_depart()const {
    cout<<"MATRICE DE DEPART:"<<endl;
    for(int i=1;i<4;i++){
        cout<<i<<"|"<<depart[i-1]<<"|"<<endl;
    }
}

double** matrice_emission(){
    double** matrice=new double*[3];
    for (int i=0;i<3;i++){
        matrice[i]=new double[4];
    }
    for (int i=1;i<4;i++){
        for(int j=1;j<5;j++){
            cout<<"Veuillez entrer la valeur de la case "<<i<<";"<<j<<" de la matrice émission:"<<endl;
            cin>>matrice[i-1][j-1];
            cout<<endl;
        }
    }
    return matrice;
}

double** matrice_transition(){
    double** matrice=new double*[3];
    for (int i=0;i<3;i++){
        matrice[i]=new double[4];
    }
    for (int i=1;i<4;i++){
        for(int j=1;j<5;j++){
            cout<<"Veuillez entrer la valeur de la case "<<i<<";"<<j<<" de la matrice transition:"<<endl;
            cin>>matrice[i-1][j-1];
            cout<<endl;
        }
    }
    return matrice;
}

double* matrice_depart(){
    double* matrice=new double[4];
    for (int i=1;i<5;i++){
        cout<<"Veuillez entrer la valeur de la case "<<i<<" de la matrice depart:"<<endl;
        cin>>matrice[i-1];
        cout<<endl;
    }
    return matrice;
}

seq::seq():sequence(new int), alphabet(new char[4]), hmm(HMM()), taille_seq(0) {
    alphabet[0]='A';
    alphabet[1]='C';
    alphabet[2]='T';
    alphabet[3]='G';
    int ETAT;
    srand(time(NULL));
    int x=rand()%100;
    
    if(x<hmm.get_depart()[0]*100){
        ETAT=0;
    }
    else{
        if(x<(hmm.get_depart()[0]+hmm.get_depart()[1])*100){
            ETAT=1;
        }
        else{
            ETAT=2;
        }
    }
    
    while(ETAT!=3){

        int v=rand()%100;
        if (v<hmm.get_emission()[ETAT][0]*100){
            sequence[taille_seq]=0;
        }
        else{
            if(v<(hmm.get_emission()[ETAT][0]+hmm.get_emission()[ETAT][1])*100){
                sequence[taille_seq]=1;
            }
            else{
                if(v<(hmm.get_emission()[ETAT][0]+hmm.get_emission()[ETAT][1]+hmm.get_emission()[ETAT][2])*100){
                    sequence[taille_seq]=2;
                }
                else{
                    sequence[taille_seq]=3;
                }
            }
        }
        taille_seq++;
        
        int w=rand()%100;
        if(w<hmm.get_transition()[ETAT][0]*100){
            ETAT=0;
        }
        else{
            if(w<(hmm.get_transition()[ETAT][0]+hmm.get_transition()[ETAT][1])*100){
                ETAT=1;
            }
            else{
                if(w<(hmm.get_transition()[ETAT][0]+hmm.get_transition()[ETAT][1]+hmm.get_transition()[ETAT][2])*100){
                    ETAT=2;
                }
                else{
                    ETAT=3;
                }
            }
        }        
    }
}

seq::seq(HMM hmm): sequence(new int), alphabet(new char[4]), hmm(hmm), taille_seq(0) {
    alphabet[0]='A';
    alphabet[1]='C';
    alphabet[2]='T';
    alphabet[3]='G';
    int ETAT;
    srand(time(NULL));
    int x=rand()%100;
    
    if(x<hmm.get_depart()[0]*100){
        ETAT=0;
    }
    else{
        if(x<(hmm.get_depart()[0]+hmm.get_depart()[1])*100){
            ETAT=1;
        }
        else{
            ETAT=2;
        }
    }
    
    while(ETAT!=3){

        int v=rand()%100;
        if (v<hmm.get_emission()[ETAT][0]*100){
            sequence[taille_seq]=0;
        }
        else{
            if(v<(hmm.get_emission()[ETAT][0]+hmm.get_emission()[ETAT][1])*100){
                sequence[taille_seq]=1;
            }
            else{
                if(v<(hmm.get_emission()[ETAT][0]+hmm.get_emission()[ETAT][1]+hmm.get_emission()[ETAT][2])*100){
                    sequence[taille_seq]=2;
                }
                else{
                    sequence[taille_seq]=3;
                }
            }
        }
        taille_seq++;
        
        int w=rand()%100;
        if(w<hmm.get_transition()[ETAT][0]*100){
            ETAT=0;
        }
        else{
            if(w<(hmm.get_transition()[ETAT][0]+hmm.get_transition()[ETAT][1])*100){
                ETAT=1;
            }
            else{
                if(w<(hmm.get_transition()[ETAT][0]+hmm.get_transition()[ETAT][1]+hmm.get_transition()[ETAT][2])*100){
                    ETAT=2;
                }
                else{
                    ETAT=3;
                }
            }
        }        
    }
}

int ::seq::getTaille()const{return taille_seq;}

void seq::afficher_seq()const {
    cout<<"Sequence:"<<endl;
    for (int i=0;i<taille_seq;i++){
        cout<<"|"<<alphabet[sequence[i]]; 
    }
    cout<<"|"<<endl;
}

long double** seq::forward() const {
    long double** alpha=new long double*[taille_seq];
    
    for(int w=0;w<taille_seq;w++){
        alpha[w]=new long double[3];
    }

    for (int ETAT=0;ETAT<3;ETAT++){
        alpha[0][ETAT]=hmm.get_depart()[ETAT]*hmm.get_emission()[ETAT][sequence[0]];
    }
    
    for(int w=1;w<taille_seq;w++){
        for(int ETAT=0;ETAT<3;ETAT++){
            alpha[w][ETAT]=(alpha[w-1][0]*hmm.get_transition()[0][ETAT]*hmm.get_emission()[ETAT][sequence[w]]) + (alpha[w-1][1]*hmm.get_transition()[1][ETAT]*hmm.get_emission()[ETAT][sequence[w]]) + (alpha[w-1][2]*hmm.get_transition()[2][ETAT]*hmm.get_emission()[ETAT][sequence[w]]);
        }
    }
    
    
    
    
    return alpha;
}

/*long double** seq::backward()const {
    long double** alpha=new long double [taille_seq];

    for(int w=0;w<taille_seq;w++){
        alpha[i]=new long double[3]
    }

    for(int w=0;w<taille_seq-1;w++){
        (int ETAT=0;ETAT<3;ETAT++){
            alpha[w][ETAT]=0
        }
    }
}*/

int main(){

    seq sequence1=seq();
    sequence1.afficher_seq(); 
    long double proba=sequence1.forward()[sequence1.getTaille()-1][0]+sequence1.forward()[sequence1.getTaille()-1][1]+sequence1.forward()[sequence1.getTaille()-1][2];
    cout<<"Prob(w|H) ="<<proba<<endl;
    for(int w=0;w<sequence1.getTaille();w++){
        for(int ETAT=0;ETAT<3;ETAT++){
            cout<<"|"<<sequence1.forward()[w][ETAT];
        }
        cout<<"|"<<endl;
    }
   
    return 0;
}





