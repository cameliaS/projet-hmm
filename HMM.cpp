#include"HMM.h"
#include<time.h>
#include<fstream>
#include<stdio.h>
using namespace std;
///////////////////////////////////////////////////  METHODES HMM  ////////////////////////////////////////////////////
HMM::HMM(): nbEtat(3), taille_alphabet(4), alphabet(new char[4]), depart(new double(3)), emission(new double*[3]), transition(new double*[3]){
    for(int i=0;i<3;i++){
        emission[i]=new double[4];
        transition[i]=new double[4];
        depart[i]=0.33;
    }

    for(int i=0;i<3;i++){
        for(int k=0;k<4;i++){
            emission[i][k]=0.25;
            transition[i][k]=0.25;
        }
    }
}

HMM::HMM(int nbEtat, int tseq, char* alpha, double* depart, double** emission, double** transition): nbEtat(nbEtat), taille_alphabet(tseq),  alphabet(alpha),depart(depart), emission(emission), transition(transition){}

int HMM::get_nbEtat()const {return nbEtat;}
int HMM::get_taille_alphabet()const {return taille_alphabet;}
char* HMM::get_alphabet()const {return alphabet;}
double* HMM::get_depart()const {return depart;}
double** HMM::get_emission()const {return emission;}
double** HMM::get_transition()const {return transition;}

///////////////////////////////////////////////////  METHODES SEQ  ////////////////////////////////////////////////////

seq::seq(): taille_sequence(0), sequence(new int), hmm(HMM()) {
      
    int ETAT=-1;
    
    srand(time(NULL));
    int roll_ETAT=rand()%100;
    int compteur=0;
    double proba_depart=hmm.get_depart()[0];
   
    while(ETAT==-1){
        
        if(roll_ETAT<proba_depart*100){
            ETAT=compteur;
        }
        compteur++;
        proba_depart+=hmm.get_depart()[compteur];

    }

   
    
    while(ETAT!=hmm.get_nbEtat()){

        int roll_emission=rand()%100;
        int i=0;
        double proba_emission=hmm.get_emission()[ETAT][0];
        while(true){
            if(roll_emission<proba_emission*100){
                sequence[taille_sequence]=i;
                taille_sequence++;
                break;
            }
            else{
                i++;
                proba_emission+=hmm.get_emission()[ETAT][i];
            }
        }

        int roll_transition=rand()%100;
        int k=0;
        double proba_transition=hmm.get_transition()[ETAT][0];
        while(true){
            if(roll_transition<proba_transition*100){
                ETAT=k;
                break;                
            }
            else{
                k++;
                proba_transition+=hmm.get_transition()[ETAT][k];
            }
        }
    }
}

seq::seq(HMM hmm): taille_sequence(0), sequence(new int), hmm(hmm) {
    int ETAT=-1;
    
    srand(time(NULL));
    int roll_ETAT=rand()%100;
    int compteur=0;
    double proba_depart=hmm.get_depart()[0];
   
    while(ETAT==-1){
        
        if(roll_ETAT<proba_depart*100){
            ETAT=compteur;
        }
        compteur++;
        proba_depart+=hmm.get_depart()[compteur];

    }

   
    
    while(ETAT!=hmm.get_nbEtat()){

        int roll_emission=rand()%100;
        int i=0;
        double proba_emission=hmm.get_emission()[ETAT][0];
        while(true){
            if(roll_emission<proba_emission*100){
                sequence[taille_sequence]=i;
                taille_sequence++;
                break;
            }
            else{
                i++;
                proba_emission+=hmm.get_emission()[ETAT][i];
            }
        }

        int roll_transition=rand()%100;
        int k=0;
        double proba_transition=hmm.get_transition()[ETAT][0];
        while(true){
            if(roll_transition<proba_transition*100){
                ETAT=k;
                break;                
            }
            else{
                k++;
                proba_transition+=hmm.get_transition()[ETAT][k];
            }
        }
    }
}


int seq::get_taille_sequence()const {return taille_sequence;}


void seq::afficher_sequence()const {
    for(int i=0;i<taille_sequence;i++){
        cout<<"|"<<hmm.get_alphabet()[sequence[i]];
    }
    cout<<"|"<<endl;
}

long double** seq::forward()const {
    int nbetat=hmm.get_nbEtat();
    long double** alpha=new long double*[taille_sequence];
    for(int w=0;w<taille_sequence;w++){
        alpha[w]=new long double[nbetat];
    }

    for (int ETAT=0;ETAT<nbetat;ETAT++){
        alpha[0][ETAT]=hmm.get_depart()[ETAT]*hmm.get_emission()[ETAT][sequence[0]];
    }
    
    for(int w=1;w<taille_sequence;w++){
        for(int ETAT=0;ETAT<nbetat;ETAT++){
            alpha[w][ETAT]=0;
            for(int k=0;k<nbetat;k++){
                alpha[w][ETAT]=alpha[w][ETAT] + hmm.get_emission()[ETAT][sequence[w]] * alpha[w-1][k] * hmm.get_transition()[k][ETAT];
            }
        }
    }
    
    
    return alpha;
}

long double seq::proba(long double** alpha)const {
    long double proba=0;
    int nbetat=hmm.get_nbEtat();
    for(int i=0;i<nbetat;i++){
        proba+=alpha[taille_sequence-1][i];
    }
    return proba;
}

long double** seq::backward()const {
    long double** alpha=new long double [taille_seq];
    int nbetat=hmm.get_nbEtat();
    for(int w=0;w<taille_seq;w++){
        alpha[w]=new long double[3]
    }

    for(int ETAT=0; ETAT<nbetat;ETAT++){
        alpha[taille_seq-1][ETAT]=hmm.get_emission()[ETAT][taille_seq-1];
        }
    for(int i=taille_seq-2;taille_seq>0;w--){
        for(int ETAT=0;ETAT<nbetat;w++){
            alpha[i][j]=0;
            for(int k=0; k<nbetat; k++){
                alpha[i][ETAT]=alpha[i][ETAT]+hmm.get_emission()[i][ETAT]*alpha[i+1][k]*hmm.get_transition[ETAT][k];
            }

        }  
    }
    for(int j=0; j<3; j++){
        alpha[0][j]=hmm.get_depart[j]*alpha[0][j];
    } 
    return alpha;   
}
/////////////////////////////////////////////////////  AUTRES   /////////////////////////////////////////////////////////////

HMM set_donnees() {
    signed char text[256];
    int nbEtat=0, taille_alphabet=0;
    char* alphabet;
    double* depart;
    double** emission;
    double** transition;

    FILE *fic = fopen("HMMdonnees.txt", "r");
    if(fic==NULL)
        exit(1);
    
    fscanf(fic,"%s %s", text, text);
    fscanf(fic,"%d", &nbEtat);
    fscanf(fic,"%s %s %s", text, text, text);
    fscanf(fic,"%d", &taille_alphabet);

    alphabet=new char[taille_alphabet];
    depart=new double[nbEtat];
    emission=new double*[nbEtat];
    transition=new double*[nbEtat];
    
    for(int i=0;i<nbEtat;i++){
        emission[i]=new double[taille_alphabet];
        transition[i]=new double[nbEtat+1];
    }

    fscanf(fic,"%s", text);
    for(int k=0;k<taille_alphabet;k++){
        fscanf(fic,"%s", &alphabet[k]);
    }
    fscanf(fic,"%s %s", text, text);
    for(int a=0;a<nbEtat;a++){        
       fscanf(fic,"%lf", &depart[a]);
    }
    fscanf(fic,"%s %s", text, text);
    for(int a=0;a<nbEtat;a++){
        for(int b=0;b<taille_alphabet;b++){
            fscanf(fic,"%lf", &emission[a][b]);
        }
    }   
    fscanf(fic,"%s %s", text, text);
    for(int a=0;a<nbEtat;a++){
        for(int b=0;b<nbEtat+1;b++){
            fscanf(fic,"%lf", &transition[a][b]);
        }
    }  

    fclose(fic);
    HMM hmm=HMM(nbEtat, taille_alphabet, alphabet, depart, emission, transition);
    return hmm;
    
}

/////////////////////////////////////////////////////   MAIN    //////////////////////////////////////////////////////////////


int main(){
    HMM hmm=HMM(set_donnees());
    seq sequence_a=seq(hmm);
    sequence_a.afficher_sequence();
    cout<<sequence_a.proba(sequence_a.forward())<<endl;
    
    return 0;
}