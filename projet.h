#include<iostream>
#include<stdlib.h>
using namespace std;

class HMM {
    private:
     double **emission ;
     double **transition ;
     double *depart ;

    public:
     HMM();
     HMM(double* depart, double **emission, double** transition);

     void set_emission(double** e);
     void set_transition(double** t);
    
     double** get_emission()const;
     double** get_transition()const;
     double* get_depart()const;
    
     void afficher_emission ()const;
     void afficher_transition ()const;
     void afficher_depart ()const;

};

class seq{
    private:
    int* sequence;
    char* alphabet;    
    HMM hmm;
    int taille_seq;

    public:
    seq();
    seq(HMM hmm);
    int getTaille()const;
    void afficher_seq() const;
    long double** forward() const;
    //long double** backward() const;
};