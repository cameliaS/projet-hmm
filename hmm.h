#include<iostream>
#include<stdlib.h>
#include<list>

class HMM {
    private:
     int nbEtat;
     int taille_alphabet;
     char *alphabet;
     double *depart;
     double **emission;
     double **transition;

    public:
     HMM();
     HMM(int nbEtat, int tseq, char* alpha, double* depart, double** emission, double** transition);

     int get_nbEtat()const;
     int get_taille_alphabet()const;
     char* get_alphabet()const;
     double* get_depart()const;
     double** get_emission()const;
     double** get_transition()const;


};

class seq{
    private:
     int taille_sequence;
     int* sequence;
     HMM hmm;

    public:
     seq();
     seq(HMM hmm);

     int get_taille_sequence()const;

     void afficher_sequence()const;

     long double** forward()const;	
     long double** viterbi()const;
     int* backtracking(long double ** delta)const;
     long double proba(long double ** alpha)const;
};
